---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
outputs : ["Reveal"]
draft: false
---
<style>
  .reveal p {
    text-align: left;
  }
  .reveal ul {
    display: block;
  }
  .reveal ol {
    display: block;
  }
</style>
