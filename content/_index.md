---
title: ""
date: 2022-12-10T19:24:23+01:00
outputs : ["Reveal"]
draft: false
---
# Liste des sujets

- [Généralités]({{< ref "/menaces/_index.md" >}})
- [RGPD]({{< ref "/rgpd/_index.md" >}})
- [RGPD - rôle des entreprises]({{< ref "/rgpd-fonctionnel/_index.md" >}})
- [Récit utilisateur]({{< ref "/user-story/_index.md" >}})
- [Sauvegardes]({{< ref "/sauvegardes/_index.md" >}})

---
## Programmation

- [PHP Introduction]({{< ref "/php/_index.md" >}})
- [PHP Formulaires]({{< ref "/formulaires/_index.md" >}})
- [Révision des Class]({{< ref "/class/_index.md" >}})
- [Création d'un DLL avec VS]({{< ref "/dll/_index.md" >}})
- [Cas d'utilisation et tests fonctionnels]({{< ref "/use-case/_index.md" >}})

---

- [Test Unitaire]({{< ref "/test-unitaire/_index.md" >}})
- [Test Unitaire avec VS]({{< ref "/testU-vs/_index.md" >}})
- [Sérialisation avec C#]({{< ref "/serialization/_index.md" >}})
- [Héritage]({{< ref "/heritage-application/_index.md" >}})
- [Héritage Cours]({{< ref "/heritage-cours/_index.md" >}})
- [Linq to Entity]({{< ref "/entity/_index.md" >}})
- [Classe Abstraite]({{< ref "/abstract/_index.md" >}})
- [Classe Interface]({{< ref "/interface/_index.md" >}})

---

- [gestion des droits]({{< ref "/sgbd-droits/_index.md" >}})
- [les transactions]({{< ref "/sgbd-transactions/_index.md" >}})
- [Dom & js]({{< ref "/domjs/_index.md" >}})

