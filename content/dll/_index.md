---
title: "Dll"
date: 2022-12-27T22:02:14+01:00
outputs : ["Reveal"]
draft: false
---
<style>
  .reveal p {
    text-align: left;
  }
  .reveal ul {
    display: block;
  }
  .reveal ol {
    display: block;
  }
</style>

# Création d'une DLL avec Visual Studio

---

Nous allons voir comment créer une dll contenant une ou plusieurs classes.  
Nous utiliserons la classe EnsembleCaracteres comme support. 


---

# Création de la DLL

---

## Création du projet
Créons un nouveau projet VS de type <mark>bibliothèque de classes (.Net Framework)</mark> que nous nommerons `libEnsCar`  :  
{{< img "images/dll/bibClass.png" "bibClass" >}}

---

Le code commence par :  
{{< img "images/dll/dllCode.png" "dllCode" >}}

---

## Ajout de documentation
La documentation est utile pour l’utilisateur des classes ; ajoutons une documentation.
Pour cela, au-dessus de la méthode (ou de la classe) à documenter, ajoutons 3 slashs, l’environnement VS va insérer 
automatiquement des balises de documentation, au-dessus du constructeur.  
Dans la balise summary, écrivons la documentation :
{{< img "images/dll/dllComment.png" "dllComment" >}}
Cette documentation apparaîtra dans l’IntelliSense quand l’utilisateur de la classe appellera le constructeur.

---

## Création de la documentation XML
Pour utiliser cette documentation, il faut générer le fichier XML associé.  
Pour cela, aller dans le menu Projet → Propriétés de libEnsCar ; la page suivante s’ouvre :
{{< img "images/dll/xml.png" "xml" >}}

---

Cocher la case « Fichier de documentation XML» :  
{{< img "images/dll/xml02.png" "xml02" >}}  
<mark>Compiler l’application.</mark>

---

Avec l’explorateur dans le répertoire de votre projet, vous trouverez la dll et le fichier XML de documentation :  
{{< img "images/dll/bin.png" "bin" >}}

---

Vous pouvez ouvrir le fichier XML et observer vos commentaires. Vous pouvez même les modifier directement dans le fichier XML.  
Ce sont les deux fichiers qu’il faudra copier dans le projet qui utilisera votre bibliothèque.


---

# Utilisation de la dll

---

La DLL peut être utilisée indifféremment dans un projet Console ou WinForms.  
La création du nouveau projet peut se faire soit :
- en créant une nouvelle solution,
- en ajoutant un projet dans la solution existante.

---

## Ajouter un projet dans une solution existante
1. Sélectionner le projet  
   {{< img "images/dll/nouveauProjet.png" "nouveauProjet" >}}
2. faire un clic droit => Ajouter => Nouveau Projet
3. Choisir le type de projet (Console ou WinForms) puis valider

---

4. Faire un clic droit sur le projet  
{{< img "images/dll/projetDemar01.png" "projetDemar01" >}}  
5. Choisir "définir en tant que projet de démarrage "
{{< img "images/dll/projetDemar.png" "projetDemar" >}}

---

6. Affichez les dépendances du projet.  
7. Faire un clic droit → "ajouter une référence au projet"  
{{< img "images/dll/referencePlus.png" "referencePlus" >}}  
8. Choisissez la DLL EnsCar   
{{< img "images/dll/referencePlus02.png" "referencePlus02" >}}

---

9. Il ne reste plus qu’à indiquer dans votre code, grâce à la clause <mark>using</mark>, la bibliothèque :  
{{< img "images/dll/usingLib.png" "usingLib" >}}  
<mark>Attention</mark> : la bibliothèque à indiquer dans la clause using est le nom du namespace de la dll ! Dans notre cas la dll et le nom du namespace sont les mêmes.

---

## Ajouter un projet en créant une nouvelle solution
1. Dans le répertoire de la solution ajouter les deux fichiers dll et xml.  
{{< img "images/dll/ajout01.png" "ajout01" >}}

---

2. Dans l’explorateur de solution déplier les références :  
   {{< img "images/dll/ajout02.png" "ajout02" >}}
3. Faire un clic droit → "ajouter une référence au projet"
4. Suivre les cas 8 et 9 précédents.

---

# ↩️

#### [Start over]({{< ref "/_index.md" >}})  